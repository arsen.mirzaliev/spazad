import MobileMenu from "./MobileMenu";

const $mobileMenuOpen = document.querySelector('#headerMobileOpen')
const $mobileMenuClose = document.querySelector('#headerMobileClose')
const $hideTopBanner = document.querySelector('#hideTopBanner')

function addEvents() {
    $mobileMenuOpen.addEventListener('click', () =>  {
      document.documentElement.style.overflow = 'hidden'
      document.querySelector('#sectionHeaderMobile').classList.add('header-mobile_active')
    })

    $mobileMenuClose.addEventListener('click', () =>  {
      document.documentElement.style.overflow = ''
      document.querySelector('#sectionHeaderMobile').classList.remove('header-mobile_active')
    })

    $hideTopBanner.addEventListener('click', () =>  {
      document.querySelector('#topInfoBanner').style.display = 'none'
    })
  }
function render() {
  MobileMenu()
}

export default function () {
  addEvents();
  render()
}

