
/**
 *
 */

//const Header = require('./Header')

import Header from './Header';


function addNavListFixWidthAndHeight(){
  const lists = document.querySelectorAll('ul.nav-module_only-desktop');

  if(lists){
    const margin = parseFloat(window.getComputedStyle(lists[0].firstElementChild).marginBottom)
    const marginLeft = parseFloat(window.getComputedStyle(lists[0].lastElementChild).marginLeft)
    const li_h = lists[0].firstElementChild.clientHeight
    const li_w = lists[0].firstElementChild.clientWidth



    if (window.NodeList && !NodeList.prototype.forEach) {
      NodeList.prototype.forEach = Array.prototype.forEach;
    }

    lists.forEach(function(ul, index)  {
        const ul_length = ul.children.length

        let _h
        let _w
        if(ul_length <= 6) {
         _h = ul_length * (margin + li_h)
         _w = li_w
        }
        if(ul_length > 6) {
           _h = 6 * (margin + li_h)
          const rowNumber = Math.ceil(ul_length / 6)
          _w = (rowNumber * li_w) + marginLeft
        }
        if(!!window.MSInputMethodContext && !!document.documentMode){
          _h += 3
        }
        ul.style.height = _h + 'px'
        ul.style.width = _w + 'px'
    })
  }
}
document.addEventListener("DOMContentLoaded", function(event) {
 addNavListFixWidthAndHeight()
 Header()
});

if(localStorage.getItem('theme')){
  document.body.classList.add('theme--dark')
}else{
  document.body.classList.add('theme--default')
}

let changeTheme = document.getElementById('changeTheme')

changeTheme.onclick = (e) => {
    if(changeTheme.checked) {
      document.body.classList.remove('theme--default')
      document.body.classList.add('theme--dark')
      localStorage.removeItem('theme')
      localStorage.setItem('theme', 'theme--dark')
    }else{
      document.body.classList.remove('theme--dark')
      document.body.classList.add('theme--default')
      localStorage.removeItem('theme')
    }
}


if (module.hot) {
  module.hot.accept(function () {
    addNavListFixWidthAndHeight()
  });
}

const svg = require('./svguse')
svg.svguse()
