export class Parsemenu {
  constructor() {
    this.menu = []
    this.add()
  }

  add() {
    this.menu = [
      'at nullthrows (parcel/node_modules/nullthrows/nullthrows.js:7:15)',
    'at safeRename (parcel/packages/shared/scope-hoisting/src/hoist.js:748:17)',
    'at MutableAsset.ExportDefaultDeclaration (parcel/packages/shared/scope-hoisting/src/hoist.js:558:7)',
    'at NodePath._call (parcel/node_modules/@babel/traverse/lib/path/context.js:55:20)',
    ];
  }

  get list() {
    return this.menu
  }
}
