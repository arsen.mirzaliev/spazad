const liNodes = document.querySelectorAll('#navModuleList > li')
let menu = [];


/**
 *
 * @param nodeChildren
 * @returns {{item: string, children: []}}
 */
function getArrayWithChildren(nodeChildren) {
  let children = [];
  nodeChildren[1].querySelectorAll('a').forEach((t) => children.push(t.textContent))
  return {
    'item': nodeChildren[0].textContent,
    children
  }
}


function renderMenu() {
  let _menu =  menu.reduce((html, link, index) => html += generateItem(link, index), '')
  document.querySelector('#headerMobileNav').innerHTML = format(_menu)
  addEventListener();
}

function addEventListener() {
  const collapseOpenButton =  document.querySelectorAll('.header-mobile-item-summary > span')
  collapseOpenButton.forEach((span) => {
    span.addEventListener('click', (e) => {
      if( e.path[3].classList.contains('header-mobile-item_active')) {
        e.path[3].classList.remove('header-mobile-item_active')
        return false
      }
      e.path[3].classList.add('header-mobile-item_active')
    })
  })
}

function generateItem(link, index) {
  const size = Object.keys(link).length
  return `
        <div class="header-mobile-item">
            <div class="header-mobile-item-summary">
                <a href="">${ link.item }</a>
                ${ size >= 2 ? `<span>
                  <svg class="icon">
                     <use xlink:href="/icons.2315c21c.svg#arrow-bottom"></use>
                  </svg>
                </span>` : `` }
            </div>
            ${ size >= 2 ? `
              <div class="header-mobile-item-details">
                 ${generateDetailsLink(link.children)}
              </div>
            ` : ''}
        </div>
    `
}


function generateDetailsLink(links) {
  return links.reduce((html, link, index) => html += `
      <a href="" class="header-mobile-item__link">${link}</a>
    `, '')
}


function format(html) {
  let tab = '\t';
  let result = '';
  let indent= '';

  html.split(/>\s*</).forEach(function(element) {
    if (element.match( /^\/\w/ )) {
      indent = indent.substring(tab.length);
    }

    result += indent + '<' + element + '>\r\n';

    if (element.match( /^<?\w[^>]*[^\/]$/ )  ) {
      indent += tab;
    }
  });

  return result.substring(1, result.length-3);
}


/**
 *
 */
function parse() {
  liNodes.forEach((li, index ) => {

    if(li.classList.contains('nav-module__item_adaptive')) {
      return false
    }

    if(li.children.length === 2) {
      menu.push(getArrayWithChildren(li.children))
    }else {
      menu.push({
        "item":  li.children[0].textContent
      })
    }
  })

  renderMenu();
}


export default function () {
  parse()
}


